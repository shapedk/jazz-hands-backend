const WebSocket = require("ws")
const express = require("express")
const app = express()

app.use(express.static(`${__dirname}/public`))
app.listen(3000, () => console.log("Listening on port 3000"))

const wss = new WebSocket.Server({
  port: 8080
})

const broadcast = data => {
  console.log("Broadcasting data", data)

  wss.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data)
    }
  })
}

const makePressedCountMessage = () => JSON.stringify({
  currentTime: Date.now(),
  pressedCount: Array.from(wss.clients).filter(client => client.pressed).length
})

const onClientConnected = client => {
  console.log("Client connected, current clients:", wss.clients.size)
  client.pressed = false
  broadcast(makePressedCountMessage())
}

const onClientDisconnected = client => {
  console.log("Client disconnected, current clients:", wss.clients.size)
  broadcast(makePressedCountMessage())
}

const onPressedEvent = (client, pressed) => {
  console.log("Received pressed event from client", pressed)
  client.pressed = pressed
  broadcast(makePressedCountMessage())
}

wss.on("connection", client => {
  client.alive = true
  onClientConnected(client)

  client.on("pong", () => client.alive = true)

  client.on("close", () => onClientDisconnected(client))

  client.on("message", message => {
    try {
      const data = JSON.parse(message)
      onPressedEvent(client, data.pressed)
    }
    catch (error) {
      console.log("Failed to call message handler", error)
    }
  })
})

setInterval(() => {
  wss.clients.forEach(client => {
    if (!client.alive) {
      client.terminate()
    }

    client.alive = false
    client.ping(() => {})
  })
}, 5000)
