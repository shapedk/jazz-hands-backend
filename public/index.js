class WrappedWebSocket {
  constructor() {
    this.setupWebSocket()
    this.messageHandlers = new Map()
  }

  setupWebSocket() {
    this.webSocket = new WebSocket(`ws://${window.location.hostname}:8080`)
    this.webSocket.onerror = this.onError.bind(this)
    this.webSocket.onclose = this.onClose.bind(this)
    this.webSocket.onmessage = this.onMessage.bind(this)
  }

  sendMessage(message) {
    if (this.webSocket.readyState !== WebSocket.OPEN) {
      console.log("Failed sending message", message)
      return
     }

    this.webSocket.send(JSON.stringify(message))
  }

  addMessageHandler(name, callback) {
    this.messageHandlers.set(name, callback)
  }

  onError(event) {
    console.log("Socket encountered error, closing socket")
  }

  onClose(event) {
    console.log("Socked closed, reconnecting")
    setTimeout(() => this.setupWebSocket(), 500)
  }

  onMessage(message) {
    console.log("Received message", message.data)

    this.messageHandlers.forEach((name, callback) => {
      try {
        callback(JSON.parse(message.data))
      }
      catch (error) {
        console.log("Failed to call message handler", name, error)
      }
    })
  }
}

class WrappedAudio {
  constructor() {
    this.audio = new Audio("https://s3.eu-central-1.amazonaws.com/jazz-hands/epic_sax_guy.mp3")
    this.audio.loop = true
  }

  play() {
    this.audio.play()
  }

  pause() {
    this.audio.pause()
  }

  seekTo(time) {
    if (this.audio.duration === undefined) {
      return
    }

    const loopedTime = time / 1000 % this.audio.duration

    if (Math.abs(this.audio.currentTime - loopedTime) > 0.25) {
      this.audio.currentTime = loopedTime
      console.log("Seeking to", loopedTime)
    }
  }
}

class Vibrator {
  constructor() {
    this.active = false
    setInterval(() => this.active && this.vibrate(200), 200)
  }

  start() {
    this.active = true
  }

  stop() {
    this.active = false
  }

  vibrate(pattern) {
    if ('vibrate' in navigator) {
      return navigator.vibrate(pattern)
    }

    if ('oVibrate' in navigator) {
      return navigator.oVibrate(pattern)
    }

    if ('msVibrate' in navigator) {
      return navigator.msVibrate(pattern)
    }

    if ('webkitVibrate' in navigator) {
      return navigator.webkitVibrate(pattern)
    }
  }
}

const pressedCounter = document.getElementById("pressed-counter")
const jazzButton = document.getElementById("jazz-button")

const webSocket = new WrappedWebSocket()
const audio = new WrappedAudio()
const vibrator = new Vibrator()

webSocket.addMessageHandler(message => {
  audio.seekTo(message.currentTime)
  pressedCounter.innerHTML = message.pressedCount
})

const playEpicSaxGuy = () => {
  audio.play()
  vibrator.start()
  jazzButton.className = "animated-button"
  document.body.className = "animated-background"
  webSocket.sendMessage({ pressed: true })
}

const stopEpicSaxGuy = () => {
  audio.pause()
  vibrator.stop()
  jazzButton.className = ""
  document.body.className = ""
  webSocket.sendMessage({ pressed: false })
}
